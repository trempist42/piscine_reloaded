/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 15:54:07 by rafalmer          #+#    #+#             */
/*   Updated: 2018/11/20 16:07:30 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

int		main(int argc, char **argv)
{
	int		i;
	int		i1;

	i = 1;
	i1 = 0;
	while (i < argc)
	{
		while (argv[i][i1] != '\0')
		{
			ft_putchar(argv[i][i1]);
			i1++;
		}
		ft_putchar('\n');
		i1 = 0;
		i++;
	}
}
