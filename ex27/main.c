/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 17:22:58 by rafalmer          #+#    #+#             */
/*   Updated: 2018/11/20 17:23:24 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int		main(int argc, char **argv)
{
	int		fd;
	int		ret;
	char	buf[BUF_SIZE + 1];

	if (argc > 2)
	{
		ft_putstr(2, "Too many arguments.\n");
		return (-1);
	}
	if (argc == 1)
	{
		ft_putstr(2, "File name missing.\n");
		return (-1);
	}
	fd = open(argv[1], O_RDONLY);
	if (fd == -1)
	{
		ft_putstr(2, "No such file or directory\n");
		return (-1);
	}
	while ((ret = read(fd, buf, BUF_SIZE)))
	{
		ft_putstr(1, buf);
	}
	return (0);
}
