/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rafalmer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 16:05:21 by rafalmer          #+#    #+#             */
/*   Updated: 2018/11/20 16:29:58 by rafalmer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	putstr(int argc, char **argv)
{
	int		i;
	int		i1;

	i1 = 1;
	i = 0;
	while (i1 != argc)
	{
		while (argv[i1][i] != '\0')
		{
			ft_putchar(argv[i1][i]);
			i++;
		}
		ft_putchar('\n');
		i = 0;
		i1++;
	}
}

int		ft_strcmp(char *s1, char *s2)
{
	int		i;

	i = 0;
	while ((s1[i] == s2[i]) && (s1[i] != '\0') && (s2[i] != '\0'))
		i++;
	return (s1[i] - s2[i]);
}

int		main(int argc, char **argv)
{
	int		i;

	i = 1;
	while (i != argc)
	{
		if (i + 1 < argc && ft_strcmp(argv[i], argv[i + 1]) > 0)
		{
			argv[argc + 1] = argv[i];
			argv[i] = argv[i + 1];
			argv[i + 1] = argv[argc + 1];
			i = 0;
		}
		i++;
	}
	putstr(argc, argv);
	return (0);
}
